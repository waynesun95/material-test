import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-donation-popup',
  templateUrl: './donation-popup.component.html',
  styleUrls: ['./donation-popup.component.scss']
})
export class DonationPopupComponent implements OnInit {

  result = 10;
  numbers = new Array<number>(5);


  constructor(public dialogRef: MatDialogRef<DonationPopupComponent>, @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close(this.result);
  }

}
