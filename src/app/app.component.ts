import { Component } from '@angular/core';
import {MatDialog} from "@angular/material";
import {DonationPopupComponent} from "./donation-popup/donation-popup.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'material-test';
  donationAmount = 0;

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(DonationPopupComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.donationAmount = result;
    });
  }

}

// button, card, menu, icons, dialog/popup, progress bar, search
